package calculator;

import exception.IllegalOperationException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    /**
     * Test for {@link Calculator#calculate(String)}
     * operation - +
     */
    @Test
    public void calculateAdditionTest(){
        String expression = "3+4";
        assertEquals(7, Calculator.calculate(expression));
    }

    /**
     * Test for {@link Calculator#calculate(String)}
     * operation - -
     */
    @Test
    public void calculateSubtractionTest(){
        String expression = "14-19";
        assertEquals(-5, Calculator.calculate(expression));
    }

    /**
     * Test for {@link Calculator#calculate(String)}
     * operation - *
     */
    @Test
    public void calculateMultiplicationTest(){
        String expression = "5*6";
        assertEquals(30, Calculator.calculate(expression));
    }

    /**
     * Test for {@link Calculator#calculate(String)}
     * operation - /
     */
    @Test
    public void calculateDivisionTest(){
        String expression = "14/4";
        assertEquals(3, Calculator.calculate(expression));
    }

    /**
     * Test for {@link Calculator#calculate(String)}
     * illegal first operand
     */
    @Test(expected = NumberFormatException.class)
    public void calculateIllegalFirstOperandTest(){
        String expression =" 1f5+14";
        Calculator.calculate(expression);
    }

    /**
     * Test for {@link Calculator#calculate(String)}
     * illegal second operand
     */
    @Test(expected = NumberFormatException.class)
    public void calculateIllegalSecondOperandTest(){
        String expression = "15+4r";
        Calculator.calculate(expression);
    }

    /**
     * Test for {@link Calculator#calculate(String)}
     * illegal operation
     */
    @Test(expected = IllegalOperationException.class)
    public void calculateIllegalOperationTest(){
        String expression = "14$15";
        Calculator.calculate(expression);
    }
}
