package calculator;

import exception.IllegalOperationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    /**
     * Calculates expression
     * @param expression Expression in form: first operand, operation, second operand (e.g '2+2','17/4')
     * @throws NumberFormatException
     * @throws IllegalOperationException
     * @return Result of expression
     */
    public static int calculate(String expression){
        Pattern p = Pattern.compile("[+\\-*/]");
        Matcher m = p.matcher(expression);
        if (!m.find()){
            throw new IllegalOperationException("No operation found in expression: "+expression);
        }
        char operation = expression.charAt(expression.split("[^0-9]")[0].length());
        int firstOperand = Integer.parseInt(expression.split("[+\\-*/]")[0]);
        int secondOperand = Integer.parseInt(expression.split("[+\\-*/]")[1]);


        switch (operation){
            case '+': return firstOperand+secondOperand;
            case '-': return firstOperand-secondOperand;
            case '*': return firstOperand*secondOperand;
            case '/': return firstOperand/secondOperand;
            default: throw new IllegalOperationException("Illegal operation:"+operation);
        }
    }
}
