package calculator;

import java.util.Scanner;

public class Main {
    public static void main(String [] args){
                Scanner sc = new Scanner(System.in);
        System.out.println("Enter expression(without spaces):");
        String expression = sc.next();
        System.out.println("Result is:");
        System.out.println(Calculator.calculate(expression));
    }
}
